#pragma once
#include <math.h>
typedef struct Vect
{
	float x;
	float y;
	float z; // for location , dir or color
};

Vect normalize_vect(Vect inVect)
{
	Vect outVect;
	float magVect = sqrt(pow(inVect.x, 2) + pow(inVect.y, 2) + pow(inVect.z, 2));
	outVect = { inVect.x / magVect, inVect.y / magVect, inVect.z / magVect };
	return outVect;
}

Vect Vect_Add(Vect VectA, Vect VectB)
{
	return { VectA.x + VectB.x, VectA.y + VectB.y, VectA.z + VectB.z };
}

Vect Vect_Mul(Vect VectA, float c)
{
	return { c*VectA.x, c*VectA.y, c*VectA.z };
}

Vect Vect_Subt(Vect VectA, Vect VectB)
{
	return Vect_Add(VectA, Vect_Mul(VectB, -1.0f));
}

float Vect_Dot(Vect VectA, Vect VectB)
{
	float out = 0;
	out = VectA.x * VectB.x + VectA.y * VectB.y + VectA.z * VectB.z;
	return out;
}