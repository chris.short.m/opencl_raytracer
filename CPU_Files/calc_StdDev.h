#pragma once

#include<math.h>
void calc_StdDev_Mean(double* data, int dataSize, float* ret_Std, float* ret_mean)
{

	double sum = 0.0f;
	float mean = 0.0f;
	float std_tmp = 0.0f;


	for (int i=0; i < dataSize; i++)
	{
		sum += data[i];
	}
	
	*ret_mean = sum / dataSize;

	for (int j = 0; j < dataSize; j++)
	{
		std_tmp += pow(data[j] - (sum/ dataSize), 2);
	}
	*ret_Std = sqrt(std_tmp / dataSize);
}