// Add you host code

#include <stdio.h>
#include <stdlib.h>
#include "CL/cl.h"
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <Windows.h>
#include "calc_StdDev.h"
#include "getCLError.h"
#include "initScene.h"
#include "cpu_rayTracing_helper.h"


#define EPSILON 0.005f
#define BOX_SCENE 1U
#define RAND_SPHERE 2U
#define SCENE_GEN RAND_SPHERE
#define ITERATIONS 5


int main()
{


	int nPlane = 0;
	int nSphere = 0;

	int imgSize = 512;
	int imgWidth = imgSize;
	int imgHeight = imgSize;
	int imgChannels = 4;

	Sphere* Sphere_ptr = (Sphere*)malloc(sizeof(Sphere)*NUM_SPHERES);
	Plane* Plane_ptr = (Plane*)malloc(sizeof(Plane)*NUM_PLANES);
	ptLightSource* ptLight_ptr = (ptLightSource*)malloc(sizeof(ptLightSource));
	char* outImg_ptr = (char*)malloc(sizeof(char)*imgWidth*imgHeight*imgChannels);

	if (SCENE_GEN==BOX_SCENE)
	{
		initScene(Sphere_ptr, Plane_ptr, ptLight_ptr, &nSphere, &nPlane);
	}
	else 
	{
		init_rand_Scene(Sphere_ptr, Plane_ptr, ptLight_ptr, &nSphere, &nPlane);
	}

	LARGE_INTEGER wind_cntr_start;
	LARGE_INTEGER wind_cntr_end;
	double run_time[ITERATIONS];
	LARGE_INTEGER cntr_frq;
	QueryPerformanceFrequency(&cntr_frq);

	for (int ix = 0; ix < ITERATIONS; ix++)
	{
		QueryPerformanceCounter(&wind_cntr_start);

		render_kernel(outImg_ptr, imgWidth, imgHeight, Sphere_ptr, Plane_ptr, ptLight_ptr, nSphere, nPlane);
		
		QueryPerformanceCounter(&wind_cntr_end);
		
		run_time[ix] = (double)(wind_cntr_end.QuadPart - wind_cntr_start.QuadPart) / cntr_frq.QuadPart;// *prof_res;
	}

	/* ~~~~~ Print out Image ~~~~~ */
	char* outputFilePath;
	if (SCENE_GEN == BOX_SCENE)
	{
		outputFilePath = "cpu_imgOut_box.jpg";
	}else{
		outputFilePath = "cpu_imgOut_randSphere.jpg";
	}

	int err;
	err = stbi_write_jpg(outputFilePath, imgWidth, imgHeight, 4, outImg_ptr, 100);
	if (err != 1) // ?
	{
		printf("Error in stbi_write_jpg. ");
	}


	/* ~~~ Print out Run-time Stats ~~~ */

	float mean_runtime = 0.0f;
	float stdDev_runtime = 0.0f;

	calc_StdDev_Mean(run_time, sizeof(run_time) / sizeof(double), &stdDev_runtime, &mean_runtime);
    printf("Mean Runtime : %f \n", mean_runtime); // divide by 1E6 converting from ns to ms (OpenCL)
	printf("Std Deviation of Runtime : %f \n", stdDev_runtime);

	/* Free memory */
	free(Sphere_ptr);
	free(Plane_ptr);
	free(ptLight_ptr);
	free(outImg_ptr);
}