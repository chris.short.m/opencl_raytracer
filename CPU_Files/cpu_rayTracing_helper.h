#pragma once
#include "initScene.h"
#include "Vect_helpers.h"
#include <math.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include "CL/cl.h"

#define EPSILON 0.003f
#define HP_TOL 0.001f

struct Ray createCamRay(const int x_coord, const int y_coord, const int imgWidth, const int imgHeight) {

	/* Note: need to change x,y_coord to floats if we use multiple rays per pixel */
	float x = (float)x_coord / (float)imgWidth;  /* convert int in range [0 - width] to float in range [0-1] */
	float y = (float)y_coord / (float)imgHeight; /* convert int in range [0 - height] to float in range [0-1] */

	float aspect_ratio = (float)(imgWidth) / (float)(imgHeight);
	float x_loc = (x - 0.5f) * aspect_ratio; // fx-0.5+n%spw + jitter ; // spw is subpixel sub divisions.
	float y_loc = 0.5f - y; // fy - 0.5f + (int(n/spw)+jitter) (- int?). spw^2 rays/pixel

	/* Define 2D Image Plane: fx [-0.5,0.5], fy [-0.5, 0.5] */
	Vect pixel_pos = { x_loc, y_loc, 20.0f };

	/* create camera ray*/
	struct Ray ray;
	ray.origin = { 0.0f, 0.0f, 35.0f }; /* fixed camera position */
	ray.direction = normalize_vect(Vect_Subt(pixel_pos, ray.origin));
	//printf("ray dir: %f\n",ray.direction.xyz );

	return ray;
}


bool intersect_sphere(struct Sphere* sphere, const struct Ray* ray, float* t)
{

	Vect ray2SphereCenter = Vect_Subt(sphere->origin, ray->origin);

	/* calculate coefficients a, b, c from quadratic equation */
	/* a = 1 */
	float b = 2.0f*Vect_Dot(ray2SphereCenter, ray->direction);
	float c = Vect_Dot(ray2SphereCenter, ray2SphereCenter) - sphere->radius*sphere->radius;
	float disc = b * b - 4.0f*c;
	float sqrt_disc = sqrt(disc);

	bool hit = false;
	/* solve for t (distance to hitpoint along ray) */
	if (disc < 0)
	{
		hit = false;
	}
	else {
		if (b > sqrt_disc)
		{
			(*t) = (b - sqrt_disc) / 2.0f; // as sqrt>0, this will always be the smaller intersection ()if there are 2
		}
		else {
			(*t) = (b + sqrt_disc) / 2.0f; // as sqrt>0, this will always be the smaller intersection ()if there are 2
		}
		if ((*t) < 0)
		{
			hit = false;
		}
		else {
			hit = true;
		}
	}
	return hit;
}

bool intersect_plane(struct Plane* plane, const struct Ray* ray, float* t)
{
	Vect c = Vect_Subt(plane->pt, ray->origin); /*Draw vector from ray origin to a point on the plane */

	float dotNorm = Vect_Dot(ray->direction, plane->normal);
	*t = Vect_Dot(c, plane->normal) / dotNorm;

	/* Intersection point would then be: ray.origin + t*ray.dir */
	Vect hit_pt = Vect_Add(ray->origin, Vect_Mul(ray->direction,(*t)));

	/* Is this hit_pt valid (within bound of two bounding points of plane) */
	bool hit = true;

	hit &= (hit_pt.x >= plane->lb.x - HP_TOL && hit_pt.x <= plane->ub.x + HP_TOL);
	hit &= (hit_pt.y >= plane->lb.y - HP_TOL && hit_pt.y <= plane->ub.y + HP_TOL);
	hit &= (hit_pt.z >= plane->lb.z - HP_TOL && hit_pt.z <= plane->ub.z + HP_TOL);

	return hit;
}

unsigned int f2Uint(float x)
{
	float x_tmp = x;
	x_tmp = (x > 1) ? 1.0f : x; /* Upper Clip to 1 */
	x_tmp = (x_tmp < 0) ? 0.0f : x_tmp; /* Lower clip to 0 */
	unsigned int y = x_tmp * 255U;
	return y;
}

Vect specular_refl(struct ptLightSource* light, struct Ray* camera, Vect surf_norm, Vect int_pt)
{
	/* Should Loop through each light source */
	Vect out;
	float phong_exp = 0; /* e >= 0 only, larger leads to more narrow specular phong lob */
	struct Ray lightRay;
	lightRay.origin = light->location; /* could be light->location or intersection pt, depending on next def */
	lightRay.direction = normalize_vect(Vect_Subt(lightRay.origin, int_pt)); /* from intersection point to light source */

	struct Ray refl_ray;

	/* From Ray Tracing from the Ground Up by K. Suffern, Chapt. 15 */
	refl_ray.direction = Vect_Add(Vect_Mul((lightRay.direction), -1.0f),  Vect_Mul(surf_norm, 2.0f * Vect_Dot(surf_norm, lightRay.direction)));
	refl_ray.origin = int_pt;

	float Ks = 0.2f; /* range [0,1), this is a property of the surface */
	float r_dot_w = (Vect_Dot(refl_ray.direction, camera->direction) > 0 ? Vect_Dot(refl_ray.direction, camera->direction) : 0.0f); /* Min clip to 0 */
	float Ls = 1.0f;
	float e = 10.0f; /* Phong coefficient */

	/* Specular Out = Ks * dot(r,w) * Ls * Cl */
	out = Vect_Mul((light->color), Ks * Ls *pow(r_dot_w, e));

	return out;
}

bool inShadow(struct ptLightSource* ptLight, Vect hit_pt, struct Sphere* sphere_shad_arr_ptr, struct Plane* plane_shad_arr_ptr, int num_spheres, int num_planes)
{
	/* Draw shadow Ray from hit point to light source(s)*/
	/* Assumes only 1 light source, else we would have to loop over light srcs */

	struct Ray shadowRay;
	shadowRay.direction = normalize_vect(Vect_Subt(ptLight->location, hit_pt)); /* Leave this unnormalized, so we can check if intersection comes before or after light source */
	shadowRay.origin = Vect_Add(hit_pt , Vect_Mul(shadowRay.direction, EPSILON)); /* Avoid self-intersection */

	float t = 999.0f;
	float scaling_f = Vect_Dot(Vect_Subt(ptLight->location, hit_pt), shadowRay.direction); /* also a.x/b.x */
	bool hit = false;
	bool global_hit_flg = false; /* Can probably thrift this and just use return */

	/* Now check if this ray intersects the scene */
	for (int ix = 0; ix < num_spheres; ix++)
	{

		hit = intersect_sphere(sphere_shad_arr_ptr, &shadowRay, &t);

		if (hit && (t > 0) && (t < (scaling_f + 0.001f))) /* t<scaling_f - intersection comes between light and hit point */
		{
			global_hit_flg = hit; /* Latch in hit status */
		}
		sphere_shad_arr_ptr++;
	}

	for (int ix = 0; ix < num_planes; ix++)
	{
		hit = intersect_plane(plane_shad_arr_ptr, &shadowRay, &t);

		if (hit && (t > 0) && (t < (scaling_f + 0.001f)))
		{
			global_hit_flg = hit; /* Latch in hit status */
		}
		plane_shad_arr_ptr++;

	}
	/* Can optimize by returning earlier... if we find and intersection, no need to check other objects */
	return global_hit_flg;
}

Vect shade_Surface(struct ptLightSource* ptLight, Vect surf_norm, Vect surf_color, Vect hitpoint, bool shadow_flag)
{

	/*normal vector at hitpoint */
	Vect normal = surf_norm;
	Vect specular_clr;
	float cosine_factor = 0.0f;
	float ambient_coeff = 0.3f;

	//shadow_flag = false; //override for now, something wonky
	if (shadow_flag)
	{
		return (Vect_Mul(surf_color,ambient_coeff)); /* ambient only */
	}
	else /* Not in Shadow */
	{

		/* For each camRay-sphere intersection, draw vector int->light
		If this intersects an object, shade "black",
		else color as color*light.intensity */

		struct Ray shadeRay;
		shadeRay.direction = normalize_vect(Vect_Subt(ptLight->location, hitpoint));
		shadeRay.origin = (Vect_Add(hitpoint, Vect_Mul(shadeRay.direction, EPSILON))); /* Move slightly off of the hitpoint, so the original hitpoint is not returned */



		cosine_factor = Vect_Dot(normal, shadeRay.direction);
		cosine_factor = (cosine_factor < 0) ? 0 : cosine_factor; /* Clip to 0 */

		/* Calculate Specular Component */
		specular_clr = specular_refl(ptLight, &shadeRay, normal, hitpoint);

		return Vect_Add(Vect_Mul((surf_color),(ambient_coeff + (1.0f - ambient_coeff)*cosine_factor)), specular_clr);

	}
}

void render_kernel(char* outputImg, const int width, const int height, Sphere* Spheres_ptr_glb, Plane* Planes_ptr_glb, ptLightSource* ptLight_ptr_glb, int n_spheres, int n_planes)
{


	Sphere* Sphere_ptr_init = Spheres_ptr_glb;
	Plane* Planes_ptr_init = Planes_ptr_glb;
	ptLightSource* PtLightSrc_ptr_init = ptLight_ptr_glb;

	Vect output;
	bool hit = false;

	/* Create Light Source*/
	struct ptLightSource ptLight;
	ptLight.location = ptLight_ptr_glb->location;
	//ptLight.location = (float3)(1.0f, 1.0f, 5.0f);
	//ptLight.location = (float3)(0.0f, 1.0f, 50.0f); /* useful for debugging, esp. specular */
	ptLight.intensity = ptLight_ptr_glb->intensity;
	ptLight.color = ptLight_ptr_glb->color;

	for (int y_coord = 0; y_coord < height; y_coord++)
	{
		for (int x_coord = 0; x_coord < width; x_coord++ ) 
		{

			/* create a camera ray */
			struct Ray camray = createCamRay(x_coord, y_coord, width, height);

			/* intersect ray with sphere */
			float t = -1;
			float t_min = 1e20; /* a big number */
			float t2 = -1;
			struct Sphere* closest_sphere_ptr;
			struct Plane* closest_plane_ptr;

			bool hit2 = false;
			bool global_hit_flg = false;

			/* Closest Obj Properties */
			Vect surf_norm;
			Vect obj_color;
			Vect hitpoint;

			/* Find closest object to camera Ray */
			for (int ix = 0; ix < n_spheres; ix++)
			{


				hit = intersect_sphere(Spheres_ptr_glb, &camray, &t);

				if (hit && (t > 0) && (t < t_min)) /*Closer than previous spheres ? */
				{
					t_min = t;

					hitpoint = Vect_Add(camray.origin, Vect_Mul(camray.direction, t_min));
					surf_norm = normalize_vect(Vect_Subt(hitpoint, Spheres_ptr_glb->origin));
					obj_color = Spheres_ptr_glb->color;

					global_hit_flg = hit; /* Latch in hit status */

				}
				Spheres_ptr_glb++;
			}
			Spheres_ptr_glb = Sphere_ptr_init;

			for (int ix = 0; ix < n_planes; ix++)
			{
				hit2 = intersect_plane(Planes_ptr_glb, &camray, &t2);

				if (hit2 && (t2 < t_min))
				{
					t_min = t2;

					hitpoint = Vect_Add(camray.origin, Vect_Mul(camray.direction, t_min));
					surf_norm = Planes_ptr_glb->normal;
					obj_color = Planes_ptr_glb->color;

					/* For two-sided surfaces flip normal if viewRay dot norm > 0 */
					/* Note: k Suffern uses the opposite dir for cameraRay, so in the book viewRay dot norm < 0 */
					surf_norm = (Vect_Dot(camray.direction, surf_norm) > 0) ? Vect_Mul(surf_norm, -1) : (surf_norm);

					global_hit_flg = hit2; /* Latch in hit status */

				}
				Planes_ptr_glb++;

			}

			Planes_ptr_glb = Planes_ptr_init;

			/* Now determine pixel color based on closest camera Ray intersection */
			if (global_hit_flg == false)
			{

				/* If no intersection, return a background color */
				output = { 0.0f, 0.1f, 1.0f };

			}
			else /*  intersection */
			{
				/* Determine if ray-scene intersection is in a shadow cast by pt light source and scene */
				bool inShadow_flg = false;
				inShadow_flg = inShadow(&ptLight, hitpoint, Spheres_ptr_glb, Planes_ptr_glb, (int)n_spheres, (int)n_planes);
				/* This flag should be passed into shade_surface function? */
				/* shadow function seems directionally right, but need to add in logic for 2 sides planes */
				output = shade_Surface(&ptLight, surf_norm, obj_color, hitpoint, inShadow_flg);
			}


			/* Write to Output */

			unsigned int uint_out[] = { f2Uint(output.x), f2Uint(output.y), f2Uint(output.z), 1U };

			for (int cnt = 0; cnt < 4; cnt++)
			{
				*outputImg = uint_out[cnt];
				outputImg++;
			}


		} // end for x_coord
	} // end for y_coord
	


}


