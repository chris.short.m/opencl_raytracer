#pragma once
#include "Vect_helpers.h"

#define WHITE_PIXEL {1.0f, 1.0f, 1.0f}
#define BLACK_PIXEL {0.0f, 0.0f, 0.0f}
#define RED_PIXEL {1.0f, 0.0f, 0.0f}
#define GREEN_PIXEL {0.0f, 1.0f, 0.0f}
#define BLUE_PIXEL {0.0f, 0.0f, 1.0f}

#define BOX_SCENE 1
#define RAND_SPHERE_SCENE 2
#define NUM_SPHERES 35
#define NUM_PLANES 5


typedef struct Sphere
{
	float radius;
	Vect origin;
	Vect color; /* float3 or float4 to define the RGB/RGBa color of the sphere*/
	Vect emission_type; /* placeholder for now */
}Sphere;

typedef struct Ray
{
	Vect origin;
	Vect direction;
}Ray;

typedef struct Plane
{
	Vect normal; /* Normal Vector to Plane */
	Vect pt;
	Vect lb; /* Creating bounding box for plane with pts 1 and 2 */
	Vect ub;
	Vect color;
	Vect emission_type;

}Plane;

typedef struct ptLightSource
{

	Vect location;
	float intensity;
	Vect color;

}ptLightSource;

void initScene(struct Sphere* sphere_ptr, struct Plane* plane_ptr, struct ptLightSource* ptLight_ptr, int* nSphere, int* nPlane)
{

	/* Create Light Source*/
	ptLight_ptr->location= { 0.800f, -0.80f, 5.0f };
	//ptLight_ptr->location = (cl_float3)(0.0f, 1.0f, 50.0f); /* useful for debugging, esp. specular */
	ptLight_ptr->intensity = 1.0f;
	ptLight_ptr->color = WHITE_PIXEL;

	/* Green Sphere */
	sphere_ptr->radius = 0.5f;
	sphere_ptr->origin = { 1.0f, 1.0f, 0.0f };
	sphere_ptr->color = { 0.0f, 1.0f, 0.0f };
	sphere_ptr++;
	(*nSphere)++;

	/* Red Sphere */
	sphere_ptr->radius = 0.2f;
	sphere_ptr->origin = { 0.0f, 0.0f, 4.0f };
	sphere_ptr->color = {1.0f, 0.0f, 0.0f};
	sphere_ptr++;
	(*nSphere)++;

	sphere_ptr->radius = 1.0f;
	sphere_ptr->origin = { 0.0f, -1.5f, 4.0f };
	sphere_ptr->color = BLUE_PIXEL;
	sphere_ptr++;
	(*nSphere)++;

	sphere_ptr->radius = 0.15f;
	sphere_ptr->origin = { 0.3f, -0.5f, 2.0f };
	sphere_ptr->color = { 0.0f, 1.0f, 1.0f };
	sphere_ptr++;
	(*nSphere)++;

	plane_ptr->normal = { 0.0f, 1.0f, 0.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = { 0.0f, -1.0f, 0.0f }; /* Point on a plane */
	plane_ptr->ub = { 1.0f, 1.0f, 5.0f };
	plane_ptr->lb = { -1.0f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color = { 1.0f, 1.0f, 0.0f };
	plane_ptr++;
	(*nPlane)++;

	plane_ptr->normal = { 1.0f, 0.0f, 0.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = { -1.0f, 0.0f, 0.0f }; /* Point on a plane */
	plane_ptr->ub = {1.87f, 1.0f, 5.0f };
	plane_ptr->lb = { -1.87f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color = { 1.0f, 1.0f, 1.0f};
	plane_ptr++;
	(*nPlane)++;

	plane_ptr->normal = { 1.0f, 0.0f, 0.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = { 1.0f, 0.0f, 0.0f }; /* Point on a plane */
	plane_ptr->ub = {1.87f, 1.0f, 5.0f };
	plane_ptr->lb = { -1.87f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color = { 0.8f, 0.8f, 0.8f };
	plane_ptr++;
	(*nPlane)++;

	plane_ptr->normal = { 0.0f, 0.0f, 1.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = { 0.0f, 0.0f, -1.0f }; /* Point on a plane */
	plane_ptr->ub = { 1.0f, 1.0f, 5.0f };
	plane_ptr->lb = { -1.0f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color =  {0.8f, 0.0f, 0.8f };
	plane_ptr++;
	(*nPlane)++;

	plane_ptr->normal = { 0.0f, 1.0f, 0.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = { 0.0f, 1.0f, 0.0f }; /* Point on a plane */
	plane_ptr->ub = { 1.0f, 1.0f, 5.0f };
	plane_ptr->lb = { -1.0f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color = {1.0f, 0.0f, 0.1f };
	(*nPlane)++;

}

void init_rand_Scene(struct Sphere* sphere_ptr, struct Plane* plane_ptr, struct ptLightSource* ptLight_ptr, int* nSphere, int* nPlane)
{

	/* Create Light Source*/
	ptLight_ptr->location = { 0.800f, -0.80f, 5.0f };
	//ptLight_ptr->location = (cl_float3)(0.0f, 1.0f, 50.0f); /* useful for debugging, esp. specular */
	ptLight_ptr->intensity = 2.0f;
	ptLight_ptr->color = WHITE_PIXEL;


	/* Note this scene is the same every time, as I am not passing a seed which changes*/
	for (int ix = 1; ix <= NUM_SPHERES; ix++)
	{
		sphere_ptr->radius = (rand() % 100) / 200.0f;
		sphere_ptr->origin = { ((rand() % 100) / 50.0f) - 1.0f, ((rand() % 100) / 100.0f) - 0.5f, ((rand() % 100) / 50.0f) - 1.0f };
		sphere_ptr->color = { (rand() % 100) / 100.0f, (rand() % 100) / 100.0f, (rand() % 100) / 100.0f }; /* rand, 0 to 1 */
		sphere_ptr++;
		(*nSphere)++;
	}
	*nPlane = 0;
}