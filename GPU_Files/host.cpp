// Add you host code
#include <stdio.h>
#include <stdlib.h>
#include "CL/cl.h"
#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include "read_source.h"
#include <Windows.h>
#include "calc_StdDev.h"
#include "getCLError.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include "initScene.h"


#define EPSILON 0.005f
#define BOX_SCENE 1U
#define RAND_SPHERE 2U
/* Define Scene to Generate */
#define SCENE_GEN RAND_SPHERE
/* Define Number of Iterations*/
#define ITERATIONS 5
/* Run over all work size configurations ? */
#define RUN_WK_SZ_OPT FALSE
/* Choose Performance Profiling Option*/
#define WPC 1U
#define OpenCL 2U
#define PROFILING_OPT WPC
/* Choose GPU */
/* May need to change GPU selected if not running on one of the selected GPUs */
#define CHRIS_LAPTOP 1U
#define LAB_MACHINE 2U
#define MACHINE CHRIS_LAPTOP 

int main(int argc, char** argv)
{

	bool opt_gpu_flg = 1; /* Choose to run Work group size optimizer*/

	/* Select Kernel to Run */
	//char* kernelName = "img_rotate"; // other: img_conv_filter
	char* kernelName = "render_kernel";
	char* kernelSrcName = "rayTracing_basic.cl";


#define CHRIS_LAPTOP 1

	cl_platform_id * platforms = NULL;
	cl_platform_id selPlatID = NULL;

	cl_uint num_platforms = 0;

	cl_int clStatus = clGetPlatformIDs(0, NULL, &num_platforms); // query to get size of platforms

	if (clStatus != CL_SUCCESS)
	{
		printf("Error in clGetPlatform ID. \n ");
	}

	platforms = (cl_platform_id*)malloc(sizeof(cl_platform_id)*num_platforms); // allocate size based on query
	clStatus = clGetPlatformIDs(num_platforms, platforms, NULL); // now actually write to numplat_forms / platforms
	if (clStatus != CL_SUCCESS)
	{
		printf("Error in clGetPlatform ID. \n ");
	}

	cl_device_id *device_list = NULL;
	cl_device_id selDevID = NULL;

	int devInd = 0;
	cl_uint num_devices = 0;
	char* platformName = NULL;
	char* deviceName = NULL;
	char* seldevName = NULL;
	size_t param_size = NULL;
	size_t dev_mx_wi_sz[3];
	cl_uint tmp = 1;


	cl_context context;
	cl_int err;
	char* kernelsrcStr;
	cl_program program;
	cl_command_queue cmdQueue;
	cl_kernel kernel;
	size_t srcFileSz;
	cl_event enqueueEvent;
	float* mapMemPtr = NULL;


	printf("Num Platform: %d \n", num_platforms);

	for (int i = 0; i < num_platforms; i++)
	{

		clStatus = clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, NULL, NULL, &param_size);

		if (clStatus != CL_SUCCESS)
		{
			printf("Error in clGetPlatformInfo. \n ");
		}

		platformName = (char *)malloc(param_size * sizeof(char));
		clStatus = clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, param_size, platformName, NULL);

		if (clStatus != CL_SUCCESS)
		{
			printf("Error in clGetPlatformInfo. \n ");
		}

		printf("	Platform Name: %s \n", platformName);

		if (strcmpi(platformName, "Intel(R) OPENCL") == 0) /* Equivalent strings. remember weird stcmpi return?*/
		{
			selPlatID = platforms[i];
			printf("Selected Platform: %s \n", platformName);
		}



		clStatus = clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
		if (clStatus != CL_SUCCESS)
		{
			printf("Error in clGetDeviceIDs. \n ");
		}


		device_list = (cl_device_id *)malloc(sizeof(cl_device_id)*num_devices);
		clStatus = clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_ALL, num_devices, device_list, NULL);
		if (clStatus != CL_SUCCESS)
		{
			printf("Error in clGetDeviceIDs. \n ");
		}

		printf("Number of Devices: %d \n", num_devices);


		for (int j = 0; j < num_devices; j++)
		{

			clStatus = clGetDeviceInfo(device_list[j], CL_DEVICE_NAME, 0, NULL, &param_size);
			if (clStatus != CL_SUCCESS)
			{
				printf("Error in clGetDeviceInfo. \n ");
			}

			deviceName = (char *)malloc((param_size) * sizeof(char));
			clStatus = clGetDeviceInfo(device_list[j], CL_DEVICE_NAME, param_size, deviceName, NULL);
			if (clStatus != CL_SUCCESS)
			{
				printf("Error in clGetDeviceInfo. \n ");
			}


			/* Select Device based on cond compile */
#if (MACHINE == CHRIS_LAPTOP)
			if ((strcmpi(deviceName, "Intel(R) HD Graphics 620") == 0)) /*Chris' local machine */
#else
			if ((strcmpi(deviceName, "Intel(R) HD Graphics 530") == 0) || (strcmpi(deviceName, "Intel(R) UHD Graphics 630") == 0))
			//if ((strcmpi(deviceName, "Intel(R) Core(TM) i7-8700T CPU @ 2.40GHz") == 0) || (strcmpi(deviceName, "Intel(R) Core(TM) i5-6500T CPU @ 2.50GHz") == 0)) /* Select CPU */
#endif
			{
				selDevID = device_list[j];
				devInd = j;
				printf("Selected Device: %s \n", deviceName);
			}

		}
	}



	clStatus = clGetDeviceInfo(selDevID, CL_DEVICE_NAME, 0, NULL, &param_size);
	if (clStatus != CL_SUCCESS)
	{
		printf("Error in clGetDeviceInfo. CL_DEVICE_NAME \n ");
	}
	seldevName = (char *)malloc(param_size * sizeof(char));
	clStatus = clGetDeviceInfo(selDevID, CL_DEVICE_NAME, param_size, seldevName, NULL);
	if (clStatus != CL_SUCCESS)
	{
		printf("Error in clGetDeviceInfo. CL_DEVICE_NAME \n ");
	}



	/* ~~ Create a Context ~~ */

	context = clCreateContext(0, 1, &selDevID, NULL, NULL, &err); // 0 or Null?
	if (err != CL_SUCCESS)
	{
		printf(" Error in creating context: %d", err);
		return 1;
	}

	/* ~~ Read in Kernel Source Code ~~ */
	kernelsrcStr = read_source(kernelSrcName, &srcFileSz);

	/* ~~ Build the Program Object: ~~ */
	program = clCreateProgramWithSource(context, 1, (const char**)&kernelsrcStr, &srcFileSz, &err);

	if (err != CL_SUCCESS)
	{
		printf("Error Creating Program Source \n");
		return 1;
	}


	/* ~~ Compile to create a dll ~~ */
	const char* strBldOptions = "-cl-std=CL2.0\0";
	err = clBuildProgram(program, 1, &selDevID, strBldOptions, NULL, NULL);
	// note: want to include "-cl-std=CL2.0" compiler flag

	if (err != CL_SUCCESS)
	{
		printf("Error Building program executable \n");

		size_t len;

		clGetProgramBuildInfo(program, selDevID, CL_PROGRAM_BUILD_LOG, 0, NULL, &len);

		char *errBuffer = (char*)malloc((len + 1) * sizeof(char));

		err = clGetProgramBuildInfo(program, selDevID, CL_PROGRAM_BUILD_LOG, len, errBuffer, NULL);

		//char errrBuffer[2048];
		//err = clGetProgramBuildInfo(program, selDevID, CL_PROGRAM_BUILD_LOG, len, errBuffer, NULL);

		if (err != CL_SUCCESS)
		{
			printf("Error in clGetProgramBuildInfo. Exiting.");

			return 1;
		}

		errBuffer[len] = '\0';

		printf("%c \n", errBuffer);
		// Note: sometime this may not print if the buffer is too long and the null terminator is dropped. 

		free(errBuffer);

		return 1;

	}

	/* Define output image data */
	int imgWidth, imgHeight, imgChannels;
	int desChannels = 4; /* Read/write all images as 4 channels for consistency */

	int imgSize = 2048;
	imgWidth = imgSize;
	imgHeight = imgSize;
	imgChannels = 4;
	desChannels = 4;


	float*  h_imgOutData = NULL;
	h_imgOutData = (float*)malloc(sizeof(unsigned int)*imgWidth*imgHeight*desChannels);
	cl_event prof_event;

	/* Initialize Scene */
	struct Sphere* scene_sphere_ptr;
	struct Plane* scene_plane_ptr;
	struct ptLightSource* scene_ptLight_ptr;

	scene_sphere_ptr = (Sphere*)malloc(sizeof(Sphere)*NUM_SPHERES);
	scene_plane_ptr = (Plane*)malloc(sizeof(Plane)*NUM_PLANES);
	scene_ptLight_ptr = (ptLightSource*)malloc(sizeof(ptLightSource)*1.0f);
	/*Note: would some kind of aligned malloc help here ? */

	/* Populate strucures with Scene info */
	int nS = 0;
	int nP = 0;
	if (SCENE_GEN == BOX_SCENE)
	{
		initScene(scene_sphere_ptr, scene_plane_ptr, scene_ptLight_ptr, &nS, &nP );
	}else if(SCENE_GEN == RAND_SPHERE)
	{
		init_rand_Scene(scene_sphere_ptr, scene_plane_ptr, scene_ptLight_ptr, &nS, &nP);
	}

	/* ~~ Create command queue ~~ */
	cmdQueue = clCreateCommandQueue(context, selDevID, CL_QUEUE_PROFILING_ENABLE, &err);

	if (err != CL_SUCCESS)
	{
		printf("Error in clCreateCommandQueue \n");
		printf("Error Code: %d \n", err);
		return 1;
	}

	/* ~~ Create Kernel ~~ */
	kernel = clCreateKernel(program, kernelName, &err);

	if (err != CL_SUCCESS)
	{
		printf("Error with clCreateKernel. Exiting. \n");
		return 1;
	}

	/* ~~ Create Buffer Objects ~~ */

	cl_mem cl_img_out = NULL;
	cl_mem cl_sphere_buf = NULL;
	cl_mem cl_plane_buf = NULL;
	cl_mem cl_light_buf = NULL;


	cl_image_format img_a_format;
	img_a_format.image_channel_order = CL_RGBA;
	img_a_format.image_channel_data_type = CL_UNSIGNED_INT8;

	cl_image_desc img_a_desc;
	img_a_desc.image_type = CL_MEM_OBJECT_IMAGE2D;
	img_a_desc.image_width = imgWidth;
	img_a_desc.image_height = imgHeight;
	img_a_desc.image_depth = 0;
	img_a_desc.image_array_size = 0;
	img_a_desc.image_row_pitch = 0;
	img_a_desc.image_slice_pitch = 0;
	img_a_desc.num_mip_levels = 0;
	img_a_desc.num_samples = 0;
	img_a_desc.buffer = NULL;

	const size_t origin_img_a[] = { 0,0,0 }; // for 2D depth must be 0
	const size_t region_ima_a[] = { imgWidth, imgHeight, 1 }; // for 2D depth must be 1

	/* ~~ Create Buffers on Device: ~~ */

	cl_img_out = clCreateImage(context, CL_MEM_WRITE_ONLY, &img_a_format, &img_a_desc, NULL, &err);
	if (err != CL_SUCCESS)
	{
		printf("Error with clCreateImage. Exiting. \n");
		printf("Error Code: %s \n", getCLError(err));
		return 1;
	}
	/* CHange to read only ? */
	cl_sphere_buf = clCreateBuffer(context, CL_MEM_USE_HOST_PTR, sizeof(Sphere)*NUM_SPHERES, scene_sphere_ptr, &err);
	if (err != CL_SUCCESS)
	{
		printf("Error with clCreateBuffer. Exiting. \n");
		printf("Error Code: %s \n", getCLError(err));
		return 1;
	}
	cl_plane_buf = clCreateBuffer(context, CL_MEM_USE_HOST_PTR, sizeof(Plane)*NUM_PLANES, scene_plane_ptr, &err);
	if (err != CL_SUCCESS)
	{
		printf("Error with clCreateBuffer. Exiting. \n");
		printf("Error Code: %s \n", getCLError(err));
		return 1;
	}

	cl_light_buf = clCreateBuffer(context, CL_MEM_USE_HOST_PTR, sizeof(Sphere)*NUM_SPHERES, scene_ptLight_ptr, &err);
	if (err != CL_SUCCESS)
	{
		printf("Error with clCreateBuffer. Exiting. \n");
		printf("Error Code: %s \n", getCLError(err));
		return 1;
	}

	///* ~~ Set Kernel Argument ~~ */

	err = 0;
	err = clSetKernelArg(kernel, (cl_uint)0, sizeof(cl_img_out), &cl_img_out);
	err |= clSetKernelArg(kernel, (cl_uint)1, sizeof(int), &imgHeight);		
	err |= clSetKernelArg(kernel, (cl_uint)2, sizeof(int), &imgWidth);
	err |= clSetKernelArg(kernel, (cl_uint)3, sizeof(cl_sphere_buf), &cl_sphere_buf);
	err |= clSetKernelArg(kernel, (cl_uint)4, sizeof(cl_plane_buf), &cl_plane_buf);
	err |= clSetKernelArg(kernel, (cl_uint)5, sizeof(cl_light_buf), &cl_light_buf);
	err |= clSetKernelArg(kernel, (cl_uint)6, sizeof(int), &nS);
	err |= clSetKernelArg(kernel, (cl_uint)7, sizeof(int), &nP);

	if (err != CL_SUCCESS)
	{
		printf("Error with clSetKernelArg. Exiting. \n");
		printf("Error Code: %s \n", getCLError(err));
		return 1;
	}

	/* ~~ execute the kernel ~~ */
	const size_t globalWorkSize[] = {imgHeight,imgWidth,0}; // smaller than max (256/256/256)

	size_t localWorkSize[] = {64,4,0}; // choose best work-item dim from KDF

	/* OpenCl Timing */
#if (PROFILING_OPT == OpenCL)
	cl_ulong start_time, end_time, submit_time, queued_time, prof_res;
	size_t return_bytes;

	double start_end_time[ITERATIONS];
	double queued_end_time[ITERATIONS];
	double submit_end_time[ITERATIONS];
#elif (PROFILING_OPT==WPC)
	/* WPC */
	double run_time[ITERATIONS];

	LARGE_INTEGER wind_cntr_start;
	LARGE_INTEGER wind_cntr_end;
	LARGE_INTEGER cntr_frq;
	QueryPerformanceFrequency(&cntr_frq);
#endif

#if (RUN_WK_SZ_OPT == TRUE)
	int optIter = 9; /* 2 ^8 = 256, mx work size*/
	for (int ix = 0; ix < optIter; ix++)
	{
		for (int iy = 0; iy < (optIter - ix); iy++)
		{

			localWorkSize[0] = pow(2,ix);
			localWorkSize[1] = pow(2,iy);
			localWorkSize[2] = 0;
#endif
			for (int iter_ctr = 0; iter_ctr < ITERATIONS; iter_ctr++)
			{
#if (PROFILING_OPT==WPC)
				QueryPerformanceCounter(&wind_cntr_start);
#endif
				err = clEnqueueNDRangeKernel(cmdQueue, kernel, 2, NULL, &globalWorkSize[0], &localWorkSize[0], 0, NULL, &prof_event); //returns event obj


				if (err != CL_SUCCESS)
				{
					printf("Error in clEnqueueNDRangeKernel \n");
					printf("Error Code: %s \n", getCLError(err));
					clReleaseKernel(kernel);
					clReleaseProgram(program);
					clReleaseCommandQueue(cmdQueue);
					clReleaseContext(context);
					return 1;
				}

				//err = clWaitForEvents(1, &prof_event); // wait for 1 event, event is &prof_event
				if (err != CL_SUCCESS)
				{
					printf("Error in clWaitForEvents \n");
					printf("Error Code: %s \n", getCLError(err));
				}

				err = clFinish(cmdQueue); // arg is of type cl_command_queue
#if (PROFILING_OPT == WPC)
				QueryPerformanceCounter(&wind_cntr_end);
#endif

				if (err != CL_SUCCESS)
				{
					printf("Error in clfinish \n");
					printf("Error Code: %s \n", getCLError(err));
					return 1;
				}


				//cl_ulong resolution;
				//clGetDeviceInfo(selDevID, CL_DEVICE_PROFILING_TIMER_RESOLUTION, sizeof(cl_ulong), &prof_res, NULL);
#if(PROFILING_OPT == OpenCL)
				err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_SUBMIT, sizeof(cl_ulong), &submit_time, &return_bytes);
				err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start_time, &return_bytes);
				err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end_time, &return_bytes);
				err = clGetEventProfilingInfo(prof_event, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &queued_time, &return_bytes);

				start_end_time[iter_ctr] = (double)(end_time - start_time)*1e-6; // convert to ms from ns
				queued_end_time[iter_ctr] = (double)(end_time - queued_time)*1e-6; // convert to ms from ns
				submit_end_time[iter_ctr] = (double)(end_time - submit_time)*1e-6; // convert to ms from ns
#elif (PROFILING_OPT == WPC)
				run_time[iter_ctr] = (double)(wind_cntr_end.QuadPart - wind_cntr_start.QuadPart) / cntr_frq.QuadPart;// *prof_res;
#endif			

			}

			printf("Kernel has finished ! \n");

			/* ~~~ Print out Image ~~~ */
			/* If device is not GPU, then the image was already printed to in serial conv*/
			err = clEnqueueReadImage(cmdQueue, cl_img_out, CL_TRUE, origin_img_a, region_ima_a, 0, 0, (void*)h_imgOutData, 0, NULL, NULL);
			if (err != CL_SUCCESS)
			{
				printf("Error in clEnqueueReadImage \n");
				printf("Error Code: %s \n", getCLError(err));
				return 1;
			}


			
#if (SCENE_GEN==RAND_SPHERE)
			char* outputFilePath = "gpu_renderOut_spheres.jpg";
#else
			char* outputFilePath = "gpu_renderOut_box.jpg";
#endif
			err = stbi_write_jpg(outputFilePath, imgWidth, imgHeight, desChannels, h_imgOutData, 100);
			if (err != 1) // ?
			{
				printf("Error in stbi_write_jpg. ");
			}

			/* ~~~ Print out Run-time Stats ~~~ */

			float mean_runtime = 0.0f;
			float stdDev_runtime = 0.0f;

#if (PROFILING_OPT == WPC)
			/*  WPC Performance */
			calc_StdDev_Mean(run_time, sizeof(run_time) / sizeof(double), &stdDev_runtime, &mean_runtime);
			printf("Local Work Size: %d, %d \n", localWorkSize[0], localWorkSize[1]);
			printf("Mean Runtime : %f \n", mean_runtime); // divide by 1E6 converting from ns to ms (OpenCL)
			printf("Std Deviation of Runtime : %f \n", stdDev_runtime);
#elif (PROFILING_OPT == OpenCL)
			/* OpenCl Performance */
			calc_StdDev_Mean(start_end_time, sizeof(start_end_time) / sizeof(double), &stdDev_runtime, &mean_runtime);
			printf("Local Work Size: %d, %d \n", localWorkSize[0], localWorkSize[1]);
			printf("Start-End Runtime : %f \n", mean_runtime); // divide by 1E6 converting from ns to ms (OpenCL)
			calc_StdDev_Mean(submit_end_time, sizeof(submit_end_time) / sizeof(double), &stdDev_runtime, &mean_runtime);
			printf("Submit-End Runtime : %f \n", mean_runtime); // divide by 1E6 converting from ns to ms (OpenCL)
			calc_StdDev_Mean(queued_end_time, sizeof(queued_end_time) / sizeof(double), &stdDev_runtime, &mean_runtime);
			printf("Queued-End Runtime : %f \n", mean_runtime); // divide by 1E6 converting from ns to ms (OpenCL)
#endif

#if (RUN_WK_SZ_OPT == TRUE)
		}
	}
#endif

	free(platforms);
	free(platformName);
	free(device_list);
	free(deviceName);
	free(seldevName);
	
	free(h_imgOutData); 
	free(scene_sphere_ptr);
	free(scene_plane_ptr);
	free(scene_ptLight_ptr);

	return 0;
}