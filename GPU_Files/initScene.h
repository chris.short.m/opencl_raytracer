#pragma once
#include "CL/cl.h"

#define WHITE_PIXEL cl_float4{1.0f, 1.0f, 1.0f, 1.0f}
#define BLACK_PIXEL cl_float4{0.0f, 0.0f, 0.0f, 1.0f}
#define RED_PIXEL cl_float4{1.0f, 0.0f, 0.0f, 1.0f}
#define GREEN_PIXEL cl_float4{0.0f, 1.0f, 0.0f, 1.0f}
#define BLUE_PIXEL cl_float4{0.0f, 0.0f, 1.0f, 1.0f}

#define BOX_SCENE 1
#define RAND_SPHERE_SCENE 2
#define SCENE_RENDER RAND_SPHERE_SCENE
#define NUM_SPHERES 35
#define NUM_PLANES 5


typedef struct Sphere
{
	cl_float radius;
	cl_float3 origin;
	cl_float4 color; /* float3 or float4 to define the RGB/RGBa color of the sphere*/
	cl_float3 emission_type; /* placeholder for now */
}Sphere;

typedef struct Ray
{
	cl_float3 origin;
	cl_float3 direction;
}Ray;

typedef struct Plane
{
	cl_float3 normal; /* Normal Vector to Plane */
	cl_float3 pt;
	cl_float3 lb; /* Creating bounding box for plane with pts 1 and 2 */
	cl_float3 ub;
	cl_float4 color;
	cl_float3 emission_type;

}Plane;

typedef struct ptLightSource
{

	cl_float3 location;
	cl_float intensity;
	cl_float4 color;

}ptLightSource;

void initScene(struct Sphere* sphere_ptr, struct Plane* plane_ptr, struct ptLightSource* ptLight_ptr, int* nSphere, int* nPlane)
{

	/* Create Light Source*/
	ptLight_ptr->location = cl_float3{ 0.800f, -0.80f, 5.0f };
	//ptLight_ptr->location = (cl_float3)(0.0f, 1.0f, 50.0f); /* useful for debugging, esp. specular */
	ptLight_ptr->intensity = 2.0f;
	ptLight_ptr->color = WHITE_PIXEL;

	/* Green Sphere */
	sphere_ptr->radius = 0.5f;
	sphere_ptr->origin = cl_float3{ 1.0f, 1.0f, 0.0f };
	sphere_ptr->color = cl_float4{ 0.0f, 1.0f, 0.0f, 1.0f };
	sphere_ptr++;
	(*nSphere)++;

	/* Red Sphere */
	sphere_ptr->radius = 0.2f;
	sphere_ptr->origin = cl_float3{ 0.0f, 0.0f, 4.0f };
	sphere_ptr->color = cl_float4{1.0f, 0.0f, 0.0f, 1.0f};
	sphere_ptr++;
	(*nSphere)++;

	sphere_ptr->radius = 1.0f;
	sphere_ptr->origin =  cl_float3{ 0.0f, -1.5f, 4.0f };
	sphere_ptr->color = BLUE_PIXEL;
	sphere_ptr++;
	(*nSphere)++;

	sphere_ptr->radius = 0.15f;
	sphere_ptr->origin = cl_float3{ 0.3f, -0.5f, 2.0f };
	sphere_ptr->color = cl_float4{ 0.0f, 1.0f, 1.0f, 1.0f };
	sphere_ptr++;
	(*nSphere)++;

	plane_ptr->normal = cl_float3{ 0.0f, 1.0f, 0.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = cl_float3{ 0.0f, -1.0f, 0.0f }; /* Point on a plane */
	plane_ptr->ub = cl_float3{ 1.0f, 1.0f, 5.0f };
	plane_ptr->lb = cl_float3{ -1.0f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color = cl_float4{ 1.0f, 1.0f, 0.0f, 1.0f };
	plane_ptr++;
	(*nPlane)++;

	plane_ptr->normal = cl_float3{ 1.0f, 0.0f, 0.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = cl_float3{ -1.0f, 0.0f, 0.0f }; /* Point on a plane */
	plane_ptr->ub = cl_float3{1.87f, 1.0f, 5.0f };
	plane_ptr->lb = cl_float3{ -1.87f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color = cl_float4{ 1.0f, 1.0f, 1.0f, 1.0f };
	plane_ptr++;
	(*nPlane)++;

	plane_ptr->normal = cl_float3{ 1.0f, 0.0f, 0.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = cl_float3{ 1.0f, 0.0f, 0.0f }; /* Point on a plane */
	plane_ptr->ub = cl_float3{1.87f, 1.0f, 5.0f };
	plane_ptr->lb = cl_float3{ -1.87f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color = cl_float4{ 0.8f, 0.8f, 0.8f, 1.0f };
	plane_ptr++;
	(*nPlane)++;

	plane_ptr->normal = cl_float3{ 0.0f, 0.0f, 1.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = cl_float3{ 0.0f, 0.0f, -1.0f }; /* Point on a plane */
	plane_ptr->ub = cl_float3{ 1.0f, 1.0f, 5.0f };
	plane_ptr->lb = cl_float3{ -1.0f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color =  cl_float4{0.8f, 0.0f, 0.8f, 1.0f };
	plane_ptr++;
	(*nPlane)++;

	plane_ptr->normal = cl_float3{ 0.0f, 1.0f, 0.0f };
	//plane_ptr->normal = normalize(plane_ptr->normal);
	plane_ptr->pt = cl_float3{ 0.0f, 1.0f, 0.0f }; /* Point on a plane */
	plane_ptr->ub = cl_float3{ 1.0f, 1.0f, 5.0f };
	plane_ptr->lb = cl_float3{ -1.0f, -1.0f, -5.0f }; /* bound on plane */
	plane_ptr->color = cl_float4{1.0f, 0.0f, 0.1f, 1.0f };
	(*nPlane)++;

}

void init_rand_Scene(struct Sphere* sphere_ptr, struct Plane* plane_ptr, struct ptLightSource* ptLight_ptr, int* nSphere, int* nPlane)
{

	/* Create Light Source*/
	ptLight_ptr->location = cl_float3{ 0.800f, -0.80f, 5.0f };
	//ptLight_ptr->location = (cl_float3)(0.0f, 1.0f, 50.0f); /* useful for debugging, esp. specular */
	ptLight_ptr->intensity = 2.0f;
	ptLight_ptr->color = WHITE_PIXEL;


	/* Note this scene is the same every time, as I am not passing a seed which changes*/
	for (int ix = 1; ix <= NUM_SPHERES; ix++)
	{
		sphere_ptr->radius = 0.1f + (rand() % 100) / 200.0f;
		sphere_ptr->origin = cl_float3{ ((rand() % 100) / 50.0f)-1.0f, ((rand() % 100) / 100.0f) - 0.5f, ((rand() % 100) / 50.0f) - 1.0f };
		sphere_ptr->color = cl_float4{ (rand() % 100) / 100.0f, (rand() % 100) / 100.0f, (rand() % 100) / 100.0f, 1.0f };
		sphere_ptr++;
		(*nSphere)++;
	}
	*nPlane = 0;
}