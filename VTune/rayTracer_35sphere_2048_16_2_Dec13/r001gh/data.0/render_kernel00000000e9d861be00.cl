
#define EPSILON 0.003f
#define HP_TOL 0.001f
#define WHITE_PIXEL (float4)(1.0f, 1.0f, 1.0f, 1.0f)
#define BLACK_PIXEL (float4)(0.0f, 0.0f, 0.0f, 1.0f)
#define RED_PIXEL (float4)(1.0f, 0.0f, 0.0f, 1.0f)
#define GREEN_PIXEL (float4)(0.0f, 1.0f, 0.0f, 1.0f)
#define BLUE_PIXEL (float4)(0.0f, 0.0f, 1.0f, 1.0f)

typedef struct __attribute__((packed)) Sphere 
{
	float radius;
	float3 origin;
	float4 color; /* float3 or float4 to define the RGB/RGBa color of the sphere*/
	float3 emission_type; /* placeholder for now */
}Sphere;

typedef struct __attribute__((packed)) Plane
{
	float3 normal; /* Normal Vector to Plane */
	float3 pt;
	float3 lb; /* Creating bounding box for plane with pts 1 and 2 */
	float3 ub; 
	float4 color;
	float3 emission_type;

}Plane;

typedef struct __attribute__((packed)) Ray 
{
	float3 origin;
	float3 direction;
}Ray;

typedef struct __attribute__((packed)) ptLightSource
{

	float3 location;
	float intensity;
	float4 color;

}ptLightSource;

/* Function to Generate Camera Rays from Camera(hardcoded) through pixel plane (also hard coded) */
struct Ray createCamRay(const int x_coord, const int y_coord, const int imgWidth, const int imgHeight){

	 /* Note: need to change x,y_coord to floats if we use multiple rays per pixel */
	 float x = (float)x_coord / (float)imgWidth;  /* convert int in range [0 - width] to float in range [0-1] */
	 float y = (float)y_coord / (float)imgHeight; /* convert int in range [0 - height] to float in range [0-1] */

	 float aspect_ratio = (float)(imgWidth) / (float)(imgHeight);
	 float x_loc = (x - 0.5f) * aspect_ratio; // fx-0.5+n%spw + jitter ; // spw is subpixel sub divisions.
	 float y_loc = 0.5f - y; // fy - 0.5f + (int(n/spw)+jitter) (- int?). spw^2 rays/pixel

	 /* Define 2D Image Plane: fx [-0.5,0.5], fy [-0.5, 0.5] */
	 float3 pixel_pos = (float3)(x_loc, y_loc, 20.0f);

	 /* create camera ray*/
	 struct Ray ray;
	 ray.origin = (float3)(0.0f, 0.0f, 35.0f); /* fixed camera position */
	 ray.direction = normalize(pixel_pos - ray.origin);
	 //printf("ray dir: %f\n",ray.direction.xyz );

	 return ray;
}

bool intersect_sphere(__global struct Sphere* sphere, const struct Ray* ray, float* t)
{

 float3 ray2SphereCenter = sphere->origin - ray->origin;
 
 /* calculate coefficients a, b, c from quadratic equation */
 /* a = 1 */
 float b = 2.0f*dot(ray2SphereCenter, ray->direction);
 float c = dot(ray2SphereCenter, ray2SphereCenter) - sphere->radius*sphere->radius;
 float disc = b * b - 4*c; /* discriminant of quadratic  */
 float sqrt_disc = sqrt(disc);

 bool hit = false;
 /* solve for t (distance to hitpoint along ray) */
 if (disc < 0)
 { 
	hit = false; 
 }else{ 
	if (b>sqrt_disc)
	{
		(*t) = (b - sqrt_disc)/2.0f; // as sqrt>0, this will always be the smaller intersection ()if there are 2
	}else{
		(*t) = (b + sqrt_disc)/2.0f; // as sqrt>0, this will always be the smaller intersection ()if there are 2
	}
	if ((*t) < 0)
	{
		hit = false;
	}else{
		hit = true;
	}
 }
	 return hit;	
}


bool intersect_plane(__global struct Plane* plane, const struct Ray* ray, float* t)
{ 
	float3 c = plane->pt - ray->origin; /*Draw vector from ray origin to a point on the plane */

	float dotNorm = dot(ray->direction, plane->normal);
	*t = dot(c, plane->normal)/dotNorm;

	/* Intersection point would then be: ray.origin + t*ray.dir */
	float3 hit_pt = ray->origin + (*t)*(ray->direction);

	/* Is this hit_pt valid (within bound of two bounding points of plane) */
	bool hit = true;

	hit &= (hit_pt.x >= plane->lb.x - HP_TOL && hit_pt.x <= plane->ub.x + HP_TOL);
	hit &= (hit_pt.y >= plane->lb.y - HP_TOL && hit_pt.y <= plane->ub.y + HP_TOL); 
	hit &= (hit_pt.z >= plane->lb.z - HP_TOL && hit_pt.z <= plane->ub.z + HP_TOL); 

	return hit;
}

uint f2Uint(float x)
{ 
	float x_tmp = x;
	x_tmp = (x > 1) ? 1.0f : x; /* Upper Clip to 1 */
	x_tmp = (x_tmp < 0) ? 0.0f : x_tmp; /* Lower clip to 0 */
	uint y = x_tmp*255U;
	return y;
}

float4 specular_refl(struct ptLightSource* light, struct Ray* camera, float3 surf_norm, float3 int_pt)
{
	/* Should Loop through each light source */
	float4 out;
	struct Ray lightRay;
	lightRay.origin = light->location; /* could be light->location or intersection pt, depending on next def */
	lightRay.direction = normalize(lightRay.origin - int_pt); /* from intersection point to light source */

	struct Ray refl_ray;

	/* From Ray Tracing from the Ground Up by K. Suffern, Chapt. 15 */
	refl_ray.direction = -(lightRay.direction) + 2*dot(surf_norm,lightRay.direction)*surf_norm;
	refl_ray.origin = (float3)(int_pt);

	float Ks = 0.2f; /* range [0,1), this is a property of the surface */
	float r_dot_w = (dot(refl_ray.direction, camera->direction) > 0 ? dot(refl_ray.direction, camera->direction): 0.0f); /* Min clip to 0 */
	float Ls = 1.0f;
	float e = 10.0f; /* Phong coefficient */

	/* Specular Out = Ks * dot(r,w) * Ls * Cl */
	out = Ks * Ls *pow(r_dot_w, e)*(light->color);

	return out;
}


bool inShadow(struct ptLightSource* ptLight, float3 hit_pt, __global struct Sphere* sphere_shad_arr_ptr, __global struct Plane* plane_shad_arr_ptr, int num_spheres, int num_planes)
{
	/* Draw shadow Ray from hit point to light source(s)*/
	/* Assumes only 1 light source, else we would have to loop over light srcs */

	struct Ray shadowRay;
	shadowRay.direction = normalize(ptLight->location - hit_pt); /* Leave this unnormalized, so we can check if intersection comes before or after light source */
	shadowRay.origin = hit_pt + EPSILON * shadowRay.direction; /* Avoid self-intersection */

	float t = 999.0f;
	float scaling_f =  dot((ptLight->location - hit_pt), shadowRay.direction); /* also a.x/b.x */
	bool hit = false;
	bool global_hit_flg = false; /* Can probably thrift this and just use return */

	/* Now check if this ray intersects the scene */
	for (int ix = 0; ix < num_spheres; ix++)
	{ 

		hit = intersect_sphere(sphere_shad_arr_ptr, &shadowRay, &t);

		if (hit && (t>0) && (t<(scaling_f+0.001f))) /* t<scaling_f - intersection comes between light and hit point */
		{
			global_hit_flg = hit; /* Latch in hit status */
		}
		sphere_shad_arr_ptr++;
	}

	for (int ix = 0; ix < num_planes; ix++)
	{ 
		hit = intersect_plane(plane_shad_arr_ptr, &shadowRay, &t);

		if (hit && (t>0) && (t<(scaling_f+0.001f)))
		{
			global_hit_flg = hit; /* Latch in hit status */
		}
		plane_shad_arr_ptr++;

	}
	/* Can optimize by returning earlier... if we find and intersection, no need to check other objects */
	return global_hit_flg; 
}

float4 shade_Surface(struct ptLightSource* ptLight, float3 surf_norm, float4 surf_color, float3 hitpoint, bool shadow_flag)
{ 

		/*normal vector at hitpoint */
		float3 normal = surf_norm;
		float4 specular_clr;				
		float cosine_factor = 0.0f;
		float ambient_coeff = 0.3f;

		//shadow_flag = false; //override for now, something wonky
		if (shadow_flag)
		{
			return ((float4)(surf_color)*(ambient_coeff)); /* ambient only */
		}
		else /* Not in Shadow */
		{ 

			/* For each camRay-sphere intersection, draw vector int->light
			If this intersects an object, shade "black", 
			else color as color*light.intensity */

			struct Ray shadeRay;
			shadeRay.direction = normalize(ptLight->location - hitpoint);
			shadeRay.origin = (float3)(hitpoint + (EPSILON * shadeRay.direction)); /* Move slightly off of the hitpoint, so the original hitpoint is not returned */
			/* Error: float3*0.01f is greater rank (double) than float? ... need to cast epsilon as float */
		
			cosine_factor = dot(normal, shadeRay.direction);
			cosine_factor = (cosine_factor < 0) ? 0 : cosine_factor; 

			/* Calculate Specular Component */
			specular_clr = specular_refl(ptLight, &shadeRay, normal, hitpoint);

			return (float4)(surf_color)*(ambient_coeff + (1.0f - ambient_coeff)*cosine_factor) + specular_clr;

		}
}

__kernel void render_kernel(__write_only image2d_t outputImg, int width, int height, __global Sphere* Spheres_ptr_glb, __global Plane* Planes_ptr_glb, __global ptLightSource* ptLight_ptr_glb, int n_spheres, int n_planes)
{
	//printf("In Kernel \n");

	const int work_id_x = get_global_id(0); /* the unique global id of the work item for the current pixel */
	const int work_id_y = get_global_id(1);
	const int g_sz_x = get_global_size(0);
	const int g_sz_y = get_global_size(1);

	//printf("Sphere: %i \n", sizeof(Sphere));
	//printf("Plane: %i\n", sizeof(Plane));
	//printf("LightSrc: %i \n", sizeof(ptLightSource));

	float4 output;
	int2 coords;
	bool hit = false;

	__global struct Sphere* sphere_ptr_init = Spheres_ptr_glb;
	__global struct Plane* plane_ptr_init = Planes_ptr_glb;
	__global struct Sphere* sphere_ptr_lcl = Spheres_ptr_glb;
	__global struct Plane* plane_ptr_lcl = Planes_ptr_glb;

	/* Create Light Source*/
	struct ptLightSource ptLight;
	ptLight.location = ptLight_ptr_glb->location; 
	//ptLight.location = (float3)(1.0f, 1.0f, 5.0f);
	//ptLight.location = (float3)(0.0f, 1.0f, 50.0f); /* useful for debugging, esp. specular */
	ptLight.intensity = ptLight_ptr_glb->intensity;
	ptLight.color = ptLight_ptr_glb->color;

	/* create a camera ray */
	struct Ray camray = createCamRay(work_id_x, work_id_y, width, height);

	/* intersect ray with sphere */
	float t = -1;
	float t_min = 1e20; /* a big number */
	float t2 = -1;
	struct Sphere* closest_sphere_ptr;
	struct Plane* closest_plane_ptr;

	bool hit2 = false;
	bool global_hit_flg = false;

	/* Closest Obj Properties */
	float3 surf_norm;
	float4 obj_color;
	float3 hitpoint;

	/* Find closest object to camera Ray */
	for (int ix = 0; ix < n_spheres; ix++)
	{ 

		
		hit = intersect_sphere(sphere_ptr_lcl, &camray, &t);

		if (hit && (t>0)&&(t<t_min)) /*Closer than previous spheres ? */
		{
			t_min = t;

			hitpoint = camray.origin + camray.direction * t_min;
			surf_norm = normalize(hitpoint - sphere_ptr_lcl->origin);
			obj_color = sphere_ptr_lcl->color;

			global_hit_flg = hit; /* Latch in hit status */

		}
		sphere_ptr_lcl++;
	}
	sphere_ptr_lcl = sphere_ptr_init; 

	for (int ix = 0; ix < n_planes; ix++)
	{ 
		hit2 = intersect_plane(plane_ptr_lcl, &camray, &t2);

		if (hit2 && (t2<t_min))
		{
			t_min = t2;

			hitpoint = camray.origin + camray.direction * t_min;
			surf_norm = plane_ptr_lcl->normal;
			obj_color = plane_ptr_lcl->color;

			/* For two-sided surfaces flip normal if viewRay dot norm > 0 */
			/* Note: k Suffern uses the opposite dir for cameraRay, so in the book viewRay dot norm < 0 */
			surf_norm = (dot(camray.direction, surf_norm) > 0)? -1*(surf_norm):(surf_norm);

			global_hit_flg = hit2; /* Latch in hit status */

		}
		plane_ptr_lcl++;
		
	}

	plane_ptr_lcl = plane_ptr_init;

	/* Determin if ray-scene intersection is in a shadow cast by pt light source and scene */
	bool inShadow_flg = false;
	inShadow_flg = inShadow(&ptLight, hitpoint, sphere_ptr_lcl, plane_ptr_lcl, (int)n_spheres, (int)n_planes);
	/* This flag should be passed into shade_surface function? */
	/* shadow function seems directionally right, but need to add in logic for 2 sides planes */

	/* Now determine pixel color based on closest camera Ray intersection */
	if (global_hit_flg == false)
	{ 

		/* If no intersection, return a background color */
		output = (float4)(0.0f, 0.1f, 1.0f, 1.0f);

	}
	else /*  intersection */
	{ 
		output = shade_Surface(&ptLight, surf_norm, obj_color, hitpoint, inShadow_flg);
	}


	/* Write to Output */
	coords = (int2)(work_id_x, work_id_y);

	uint4 uint_out;
	uint_out = (uint4)(f2Uint(output.x), f2Uint(output.y), f2Uint(output.z), 1U);

	write_imageui(outputImg, coords, uint_out);

}
 